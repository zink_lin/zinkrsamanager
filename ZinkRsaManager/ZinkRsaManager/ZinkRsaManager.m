//
//  RsaSignManager.m
//  TestRsaSign
//
//  Created by mac on 14-7-30.
//  Copyright (c) 2014年 林峥. All rights reserved.
//

#import "ZinkRsaManager.h"
#import "openssl_wrapper.h"
#import "DataVerifier.h"

@implementation ZinkRsaManager

+(NSString *)getRSASign:(NSString *)strSign{
    NSString * signedString = nil;
    NSString * strToSign = strSign;
    
    NSString * path = [[NSBundle mainBundle] pathForResource:@"zink_private_key" ofType:@"pem"];
    const char * message = [strToSign cStringUsingEncoding:NSUTF8StringEncoding];
    int messageLength = strlen(message);
    unsigned char * sig = (unsigned char *)malloc(256);
    unsigned int sig_len;
    int ret = rsa_sign_with_private_key_pem((char *)message, messageLength, sig, &sig_len, (char *)[path UTF8String]);
    if (ret == 1) {
        NSString * base64String = base64StringFromData([NSData dataWithBytes:sig length:sig_len]);
        
        signedString = base64String;
        

    }
    
    free(sig);
    
    return signedString;
}

+(BOOL)getRsaVerify:(NSString *)strVerify withSign:(NSString *)strSign{
    NSString * sc_public = @"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDhLPp3fmhXprZ6C63YHDieO5kNtecmvzBp8gCMUU24BL9w/EFDBZ7q9UWEbHSwAQynXwJ7YUae8hk0+v3JgTzpORuLmWYdACVyyy48703o6CrSYql1ZlW6w/WLAfXyJK9iv9aKx6h+hqQvxJwJE9sLZRePkI9rrioTLW9JfMLArwIDAQAB";
    
    id<DataVerifier> verifier = CreateRSADataVerifier(sc_public);
    
    BOOL result = [verifier verifyString:strVerify withSign:strSign];
    
    if (result) {
        NSLog(@"验签成功");
    }else{
        NSLog(@"验签失败");
    }
    
    return result;
}

+(NSString *)encryptByRsa:(NSString *)content withKeyType:(KeyType)keyType{
    CRSA * crsa = [CRSA shareInstance];
    
    NSString * strEncrypted = [crsa encryptByRsa:content withKeyType:KeyTypePublic];
    
    return strEncrypted;
}

+(NSString *)decryptByRsa:(NSString *)content withKeyType:(KeyType)keyType{
    CRSA * crsa = [CRSA shareInstance];

    
    NSString * strDecrypted = [crsa decryptByRsa:content withKeyType:KeyTypePrivate];
    
    return strDecrypted;
}

@end
