//
//  RsaSignManager.h
//  TestRsaSign
//
//  Created by mac on 14-7-30.
//  Copyright (c) 2014年 林峥. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CRSA.h"


@interface ZinkRsaManager : NSObject


 
//注意事项: 使用前在header SearchPath 中正确设置 openssl 所在路径

//rsa签名
+(NSString *)getRSASign:(NSString *)strSign;
//rsa验签
+(BOOL)getRsaVerify:(NSString *)strVerify withSign:(NSString *)strSign;

+(NSString *) encryptByRsa:(NSString*)content withKeyType:(KeyType)keyType;
+(NSString *) decryptByRsa:(NSString*)content withKeyType:(KeyType)keyType;

@end
