//
//  ViewController.m
//  ZinkRsaManager
//
//  Created by Zink on 14-9-15.
//  Copyright (c) 2014年 林峥. All rights reserved.
//

#import "ViewController.h"
#import "ZinkRsaManager.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    NSString * strSigned = [ZinkRsaManager getRSASign:@"123"];
    
    BOOL result = [ZinkRsaManager getRsaVerify:@"123" withSign:strSigned];
    
    NSString * strEecrypt = [ZinkRsaManager encryptByRsa:@"123" withKeyType:KeyTypePublic];
    NSLog(@"加密：%@", strEecrypt);
    
    NSString * strDecrypt = [ZinkRsaManager decryptByRsa:strEecrypt withKeyType:KeyTypePrivate];
    NSLog(@"解密：%@", strDecrypt);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
